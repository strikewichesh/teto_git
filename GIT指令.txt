----------------------------------------------------
※git 筆記
----------------
git 設定
$ git config (<參數>) (<設定>) <設定值>
$ git config <參數>
用於 設定&查看 git 的各種設定

常用參數設定
指定寫入設定的設定檔(區域)
    --global              使用全域預設定檔，全域作用
    --local               使用當地設定檔，僅作用在該儲存庫
動作
    -l, --list            列出所有設定的list
    -e, --edit            打開編輯器
----------------
設定識別資料
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com

#因為--global參數會直接設定全域，要只針對專案的話，可以不加--global參數
----------------
指定編輯器
$ git config --global core.editor <yourEditor>
會指定用來編輯commit訊息的編輯器，掛個用的順手的編輯器就好
----------------
設定預設的 push 設定
git config --global push.default <設定值>
設定push預設會使用哪種方式，push client端的分支
有兩種，Simple跟Matching

Matching
  Push 設定為 upstream 的 branch
Simple
  Push 當下所在的 branch，其餘 branch 不影響

#預設會使用Matching
----------------
git 版本標籤
$ git tag
預設會印出所有的標籤
加上 -l 參數，可以利用特定"pattern"(樣式)去找想要的標籤版本
  $ git tag -l "<your_pattern>"
有註解的標籤，加 -a 參數建立一個含完整資訊的標籤，-m 參數會加上註解
  $ git tag -a v1.4 -m "my version 1.4"
  建立一個完整標籤 v1.4 於目前版本，並註解 my version 1.4
  -m後面未加註解的話會打開預設編輯器提供添加訊息
建立輕量標籤，不加任何參數
  $ git tag v1.4-lw
  建立一個只指向其版本但不儲存任何額外資訊的輕量標籤v1.4-lw 於目前版本
對過去的版本添加標籤
  $ git tag -a v1.2 9fceb02 <- 針對9fceb02增加標籤v1.2
----------------
檢查設定值
$ git config --list
列出git所有設定值
----------------
初始化 git ，建立本地倉儲
$ git init
會建立.git資料夾，但不會追蹤任何檔
--bare  加上此參數，會創建一個無工作目錄的儲存庫
        通常用於單純作為 儲存&協作 的遠端儲存庫時使用
----------------
暫存、加入追蹤(add tracking)檔案
$ git add (<參數>) <file>      ← 追蹤指定檔
$ git add (<參數>) <folder>/*  ← 追蹤指定資料夾下的所有檔

-u/--update 
  更新 "正在追蹤" 的檔案
  包含了更改的、刪除的檔案
  新增的檔不算在內
-A/-all
  把所有更改都加入追蹤

#<file> 如果打 . 
 會直接將該目錄下"新增但未追蹤"跟"已追蹤且更改"的資料加入追蹤
 但不包含刪除的檔，即使他已經加入追蹤
 有加進.gitignore檔案&資料夾除外
#如果在git add之後，還沒提交，但又有更改檔案
 必須再次git add，才會追蹤新的更改資料內容進預存區 
 這個狀態下，預存會直接被最新的一次add覆蓋

 -A 跟 . 跟 -u 差別參考網頁
https://stackoverflow.com/questions/572549/difference-between-git-add-a-and-git-add?rq=1
----------------
查看 git 檔案狀態
$ git status

會列出該資料夾下所有檔的git狀態，像這樣
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working directory clean

可以加 -s / --short 參數，會變成只簡單列出所有檔的狀態

----------------
比較已預存、未預存的檔案不同
$ git diff
未加參數，可以查看目前尚未預存的，與目前工作目錄檔案or目前已預存的資料的不同處
加上參數 --staged，可以查看目前已預存的檔案，與目前的工作目錄檔案的不同處

git diff (<commit>) (<commit >)
也可以比較兩個提交(commit之間的差異
----------------
忽略不需要git的檔案
.gitignore
需要被忽略的檔案會是自動產生的檔案，或是任何你想忽略掉的檔案
建立一個.gitignore檔案，用於設定哪些檔案&目錄是不需要被git的
  $ vim .gitignore

#.gitignore文件設定規則
  空白列，或者以 # 開頭的列會被忽略。
  可使用標準的 Glob 模式。
  以斜線（/）開頭以避免路徑遞迴。（譯注：只忽略特定路徑；如果不以斜線開頭，則不管同名檔案或同名資料夾在哪一層都會被忽略。）
  以斜線（/）結尾代表是目錄。
  以驚嘆號（!）開頭表示將模式規則反向
----------------
查看 git 紀錄
$ git log 
用於查看所有動作的紀錄(log)
$ git log (<參數>)

--oneline
  每個動作單行顯示，會變得比較簡潔
--color
  以git color設定的顏色標示文字
  增加辨識度
--decorate
  顯示所有指向commit的引用(branch、tag)
--graph
  繪製一個ASCII圖形
  可以顯示commit歷史的分支結構
  常與--oneline 和 --decorate並用
--all
  印出所有log(包含branch)

參照網頁
https://github.com/geeeeeeeeek/git-recipes/wiki/5.3-Git-log-%E9%AB%98%E7%BA%A7%E7%94%A8%E6%B3%95#%E8%87%AA%E5%AE%9A%E4%B9%89%E6%A0%BC%E5%BC%8F
----------------
顯示最近的動作紀錄
$ git show
用於查看最近的動作紀錄
操作跟git log相似
----------------
提交一個版本
$ git commit -m '<your_commit_message>'
-m為訊息參數，若沒有加參數，或是沒有打上訊息，會打開設定的預設編輯器讓你加上commit訊息

--amend
  加上此參數，該次commit會更改"最後一次"的coomit紀錄
  該參數會對這個"最後一次"的coomit紀錄再做一次commit
  commit訊息也會重寫
----------------
取出(複製)指定commit資料
$ git cherry-pick <commit>
會將指定的commit資料，抽出拿到目前的commit上
但這些資料會以暫存的方式存在，還不會提交

#改寫提交
 常與commit配合，用於取部份分支的功能改寫目前的提交
----------------
取得現有的倉儲(clone)
$ git clone

HTTP方式
$ git clone https://github.com/libgit2/libgit2
SSH方式
$ git clone ssh://<host_name>@<host_ip>/<diir>/.../<your_gitname>.git

#必須確定是在有足夠權限進行更動的資料夾下做clone
 若需要sudo，建議直接去更改資料夾權限
 不然clone可能會出問題
----------------
遠端協同工作
$ git remote (<動作>)
用於操作遠端協作git
不加動作，會印出目前本機git所有的遠端分支

add - 新增遠端版本庫
  以指定別名新增指定的遠端版本庫至本地端
  $ git remote add <alias> <url>
  #需要在有.git的目錄下執行，以確定能指定給該.git
  若該目錄無.git，會出現錯誤
  可以先git init初始化一個git在該資料夾後，再指定遠端版本儲存庫

update - 更新遠端git所有分支至本機git
  $ git remote update
----------------
重做(reset)提交(commit)
$ git reset
有三個模式可設定 -m/--mixed , -s/--soft , -h/--hard 
--mixed
  預設使用，會捨棄掉暫存區檔案，但工作目錄(原始檔)不動
  也就是，commit拆出來的舊版本檔案，會直接放到工作區目錄
  暫存區則沒有檔案
--soft
  在此模式，工作目錄跟暫存區檔案都不會丟掉
  因此看起來只有 HEAD 的移動，資料不動
  所以commit拆出來的舊版本檔案，會被放到暫存區
--hard
  在此模式，工作目錄跟暫存區檔案都一併丟棄

操作方式
1.先查看git紀錄
  $ git log --oneline
    ------------------
    74e1ff8 add messageboard 0.0.1
    df8a0ee Initial Commit
    ------------------

若想重做最後一次的提交 -> 74e1ff8

2.1相對方式重做
  $ git reset 74e1ff8^ 或是
  $ git reset master^  或是
  $ git reset HEAD^
  一個 ^ 代表往前一次，所以就會倒退到 相對74e1ff8版 上一次的提交 -> df8a0ee
  達到重做最後一次提交的目的
  若要倒退到 N 個版本以前 ^ 可以改成 ~N 
2.2絕對方式重做
  若確定是要倒退到指定版本，可以直接指定該版本
  $ git reset df8a0ee
  這樣就直接退到版本 -> df8a0ee
  達到重做最後一次提交的目的

參考網頁
https://gitbook.tw/chapters/using-git/reset-commit.html
----------------
檢出
$ git checkout <commit_version or branch_name>
將HEAD指向到指定版本/分支，以便跳至該版本

-b/--branch
  加上此參數會建立一個分支並檢出該分支
-f/--force
  強行檢出(指向)
  會捨棄當下分支的所有更改
----------------
查看分支
$ git branch
會列出目前所有分支的簡易清單
-v
  查看各分支的最後一個提交(commit)
--merged
  查看那些分支被合併到目前分支/版本
--no-merged
  查看所有包含未合併工作的分支
----------------
建立分支
$ git branch <branch_name>
會在所在的版本上建立一個分支
通常會跟git checkout並用
ex:
查看紀錄
  $ git log --oneline
    ----------------------------------
    817212f 修正分頁按鈕顯示問題 使其在 .....
    5032099 main.js  fix Pagination tooLong problem && add ........
    74e1ff8 add messageboard 0.0.1
    df8a0ee Initial Commit
    ----------------------------------
#如果想跳到版本5032099，並建立一個分支 hotfix0.23
  檢出/指向版本5032099
    $ git checkout 5032099
  建立分支hotfix0.23
    $ git branch hotfix0.23
  檢出/指向分支hotfix0.23
    $ git checkout 5032099
  建立與檢出分支也可以合併成一行處理
    $ git checkout -b hotfix0.23
  確認目前狀態
    $ git status
      ----------------------------------
      On branch hotfix0.23
      nothing to commit, working directory clean
      ----------------------------------
之後就能在該分支上進行工作修改，同時不影響原版本
完成測試後，再將其 合併(merge) 回想合併的版本即可
之後不會再用到該分支，就可以用 -d 參數將該分支刪除
$ git branch -d hotfix0.23
  ----------------------------------
  Deleted branch hotfix0.23 (was 5032099).
  ----------------------------------
----------------
儲藏 儲存目前的暫存工作狀態
$ git stash
用於把當前的工作區暫存放到一個堆疊之中
但跟commit不同，不會產生任何版本資料
只是一個暫時的工作暫存堆疊
應用在手上工作未完成，不適合commit
但又須暫時跳至另一分支處理工作時用

stash可以存很多個，可用list查看目前所有的stash
$ git stash list

當回到該commit/分支工作時，可用apply套用儲存的的工作暫存
$ git stash apply (--index)
被套用的工作暫存，必須再次做git add，否則不會被追蹤
或是在apply的時候加上--index，那麼在就會一併將這些資料加進追蹤

用apply套用之後，原本的stash依然會存在，如果不會再用到
可以用drop將該stash移除
$ git stash drop <stash_name>
或是用pop方式套用stash
在套用當下就直接把該stash從堆疊中移出
----------------
合併分支
$ git merge <branch_name or commit>
合併指定分支上的最後一個commit，到目前的 版本(commit)
----------------
衍合/變基
$ git rebase <branch_name or commit>
重新定義分支的參考基準

用於合併時，與merge相似
但是當合併的分支與目前分支參考不同時
會改變分支的參考基準，以選定的分支為基礎
進行合併工作，而不是參考原本所在分支的commit來源進行合併

-i/ --interactive
  原文解釋 let the user edit the list of commits to rebase
  該參數會打開interactive模式
  該模式可以編輯 從 指定的commit往後一個 到 目前HEAD指向的commit
  之間所有commit
  
  該模式有數個方法可以指定要如何編輯commit，如下
    pick
      要這條 commit ，什麼都不改
    reword
      要這條 commit ，但要改 commit message
    edit
      要這條 commit，但要改 commit 的內容
    squash
      要這條 commit，但要跟前面那條合併，並保留這條的 messages
    fixup
      squash + 只使用前面那條 commit 的 message ，捨棄這條 message
    exec
      執行一條指令
    
  該模式會使用所設定的git預設編輯器
  打開類似commit編輯的畫面
  該畫面會列出所有可更改的commit
  預設使用pick，只要根據需要更改動作方法即可
  順序也可以更動，"刪除 任一個 commit"會使該commit遺失
  之後會依序執行每一條指定的commit更改
--onto
  指定由何處開始變基，並變基至何處
  $ git rebase --onto <current base-commit> <new base-commit>
  這樣目前分支的參考基準就會由 current base-commit 變基到 new base-commit
--continue
  接續 rebase 
  當rebase途中遇上衝突，在解決衝突之後
  可以使用該參數，繼續剛剛的rebase
  $ git-rebase -continue

interactive模式參考網頁
https://blog.yorkxin.org/2011/07/29/git-rebase.html
----------------
" 更新 "遠端儲存庫資料至本機儲存庫
$ git fetch <origin_host>
會下載遠端儲存庫的資料下來，更新本機儲存庫持有的.git資料
只能更新目前所在分支的commit，除非非加上 -all 參數
--all
  更新所有分支
----------------
" 更新並合併 "遠端儲存庫至本機儲存庫
$ git pull <origin_host>
基本上 == git fetch + git merge
合併方式也能選擇rebase，只要加上--rebase參數即可

會下載遠端儲存庫的資料下來，更新本機儲存庫持有的.git資料
同時會將遠端庫的master分支與本機的master分支合併

#如果本機跟遠端的master分支被判定為不同源，這個動作會產生一個額外的commit
#以--rebase方式合併的話，因為會以本機master分支為參考基準，所以不會產生額外的commit
----------------
如何在一個有 SSH 認證的 SERVER上，建立一個遠端儲存庫

先建立一個裸庫
  $ cd /src
  $ mkdir <git_name>.git
  $ cd <git_name>
  $ git init --bare --shared
這樣就在 /src/git/下 建立了一個<git_name>.git 裸庫
裸庫 -> 無工作目錄的儲存庫
--shared 此參數會將其資料夾組權限對應寫入git權限設定中

然後所有持有此主機 SSH 存取權限的使用者
就都能直接使用該.git裸庫，進行git操作
像是這樣
git clone ssh://<user_name>@<host_ip>/srv/git/<git_name>.git
----------------
----------------
----------------
----------------
----------------------------------------------------